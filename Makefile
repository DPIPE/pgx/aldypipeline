COMMITSHA ?= $(shell git rev-parse --short HEAD)
PROJECT ?= $(shell basename "${PWD}")
ALDY_VERSION ?= 4.6

ALDY_DOCKER_IMAGE=$(PROJECT)-$(COMMITSHA)-aldy:$(ALDY_VERSION)
ALDY_SINGULARITY_IMAGE=$(PROJECT)-$(COMMITSHA)-aldy-$(ALDY_VERSION).sif
ALDY_ARCHIVE=$(PROJECT)-$(COMMITSHA).zip

build-docker:
	docker build -t $(ALDY_DOCKER_IMAGE) --build-arg ALDY_VERSION=$(ALDY_VERSION) .

build-singularity:
	mkdir -p singularity
	singularity build singularity/$(ALDY_SINGULARITY_IMAGE) \
	docker-daemon://$(ALDY_DOCKER_IMAGE)

.PHONY: build
build: build-docker build-singularity

test-aldy:
	singularity exec singularity/$(ALDY_SINGULARITY_IMAGE) aldy test

.PHONY: test
test: test-aldy

git-archive:
	mkdir -p archive
	git archive \
	--prefix $(PROJECT)/ \
	--add-virtual-file $(PROJECT)/archive_version.txt:$(ALDY_ARCHIVE) \
	-o archive/$(ALDY_ARCHIVE) $(COMMITSHA)

singularity-shell:
	singularity shell singularity/$(ALDY_SINGULARITY_IMAGE)

.PHONY: clean
clean:
	singularity cache clean
	rm -r singularity archive