FROM python:3.12.0-slim-bookworm

RUN apt-get update && apt-get -y install \
    build-essential \
    zlib1g-dev \
    locales && \
    rm -r /var/cache/apt /var/lib/apt

WORKDIR /aldy

ENV LC_ALL="en_US.UTF-8"
ENV LC_CTYPE="en_US.UTF-8"
ENV LANG="en_US.UTF-8"

RUN dpkg-reconfigure locales

ARG ALDY_VERSION=4.4

RUN pip3 install aldy==${ALDY_VERSION}

ENTRYPOINT [ "/bin/bash" ]