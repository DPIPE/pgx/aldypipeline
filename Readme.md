# Aldy pipeline

This repository has container definition files and simple scripts to simplify the running of the [Aldy software](https://github.com/0xTCG/aldy) for pharmacogenomics.
Please make sure to read and adhere to the [Aldy license](https://github.com/0xTCG/aldy/blob/master/LICENSE.rst) before using Aldy.

The Aldy documentation can be found [here](https://aldy.readthedocs.io/en/latest/index.html)

## Building singularity (from a Dockerfile)

Make minimal singularity container for Aldy for use on the TSD cluster

(Note that you can specify the `ALDY_VERSION` to use. Otherwise the default is `4.6`)

```bash
ALDY_VERSION=4.6 make build
```

Test the new singularity build

```bash
ALDY_VERSION=4.6 make test-aldy
```

_Note: [Three tests are expected to fail for aldy for Aldy version `4.6`](https://github.com/0xTCG/aldy/issues/55)_

## Exporting the aldypipeline scripts

Make an archive of all aldypipeline files that are tracked by git

```bash
make git-archive
```

## Testing aldypipeline scripts

For testing the [aldy run script](scripts/run_aldy.sh), dry-run on empty data as

```bash
bash scripts/run_aldy.sh --BAM=test/data/empty.bam --OUTDIR=. --CONFIG=test/config/test_aldy_config.json --DRYRUN
```

For testing the [pgx analysis script](scripts/pharmacolyze.sh), dry-run on empty data as

```bash
bash scripts/pharmacolyze.sh test/input/sample_ids.txt test/config/test_pharmacolyzer_config.json --DRYRUN
```

For testing the [pgx result exporter](scripts/archive_results.sh), dry-run on empty data as

```bash
bash scripts/archive_results.sh test/input/sample_ids_zip.txt test/results/aldy_result_test.zip test/config/test_pharmacolyzer_config.json --DRYRUN
```

## Running

The repository contains [scripts](scripts/) for running Aldy on a single file and for running Aldy for all sample ids in a list.

Prerequisite to run out of the box on TSD:

- Membership in the TSD project `p22`
- Computation node should have 14GB of RAM

### Running Aldy on a single BAM or CRAM file on TSD

If running on another system, modify or replace the [`config/tsd_aldy_config.json`](config/tsd_aldy_config.json) file.

Run script as

```bash
BAM=path_to_bam_or_cram # Aldy can take both BAM and CRAM files out-of-the-box
OUTDIR=path_for_aldy_output_and_logs # For all output
bash scripts/run_aldy.sh --BAM=${BAM} --OUTDIR=${OUTDIR} --CONFIG=config/tsd_aldy_config.json [--DRYRUN]
```

### Running Aldy on all sample ids in a text file

If running on another system, modify or replace the [`config/tsd_pharmacolyzer_config.json`](config/tsd_pharmacolyzer_config.json) file.

Run script as

```bash
SAMPLE_IDS="sample_ids.txt" # File with a list of sample ids (typically from Swisslab)
bash scripts/pharmacolyze.sh ${SAMPLE_IDS} config/tsd_pharmacolyzer_config.json [--DRYRUN]
```

_Note: The sample ids are typically on form `HG12345678` or `123456789`. The sample IDs might point to multiple sample names. The progam will issue warnings, and the sample IDs might in these cases have to be manually modified to the form `wgs123-HG12345678` or `wgs123-123456789`. Sometimes, the non-`HG`-form has to be appended by `01` or `02` as this expresses different DNA extractions. Also `TRIO` samples should not cause trouble, but a patient that is also a parent might have to be specified as `HG12345678-PM` (male) or `HG12345678-PK` (female), depending on the Warning issued by the program_

## Collecting results for export

If using the `slurm` executor, the following script will make sure that all Aldy pipelines have finished before collecting the results in an encrypted 7Z file:

```bash
SAMPLE_IDS="sample_ids.txt" # File with a list of sample ids (typically from Swisslab)
ZIP_FILE="aldy_results.7z" # Name of the 7Z file to collect the results in
bash scripts/archive_results.sh ${SAMPLE_IDS} ${ZIP_FILE} test/config/test_pharmacolyzer_config.json [--DRYRUN]
```

_Note: Password for the 7Z file will be prompted interactively, unless a `PASSWORD` environment variable is set_

## Results

Results are based on publicly available datasets.

- `NA12878` is female
- `HG002` is male

Results are placed in

```bash
test/results
```

Files

- `.aldy` is the raw output of Aldy
- `.log` is the SLURM log file for the Aldy run

## Refine Aldy calling

Warnings in the log files suggest that in order to find de-novo mutations, the original variant calling (performed by Dragen, not Aldy) should be phased by an external tool. The external phasing file should then be used as input to Aldy via the `--phase` argument.

Another warning hints that the `NA12878` does not have the expected read depth. Consider looking into this.