#!/usr/bin/env bash

# This script packages the results of a pharmacolyze run into a zip file
# Example run
# ./archive_results.sh input/sample_ids.txt [--DRYRUN]

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Exit on errors
set -euo pipefail

# Check that zip is available
command -v zip >/dev/null 2>&1 || log ERROR "Command 'gzip' was not found"

#####################
# Internal functions
#####################
# Log to STDERR
# Run as: log [INFO|DEBUG|WARNING|etc] [ERR_MESSAGE]
log() {
    local TYPE="${1}"
    local MSG="${2}"
    local TIME
    TIME="$(date +%y%m%d-%H%M%S)"
    echo "${TYPE}:${TIME}:${MSG}" >&2
}

# Run command (optionally dryun)
# Run as: DRYRUN=${DRYRUN:-} run_cmd $CMD
run_cmd() {
    local CMD="${*}"
    if [[ -z ${DRYRUN} ]]; then
        log INFO "Running: ${CMD}"
        ${CMD}
        return ${?}
    else
        log DRYRUN "${CMD}"
        return 0
    fi
}

# Check if global variable PASSWORD is set
# Run as: check_password $PASSWORD
check_password() {
    local INPUT="${1}"
    if [[ -z ${INPUT} ]]; then
        log INFO "PASSWORD will be set interactively"
    else
        log INFO "Environmental variable PASSWORD is set"
    fi
}

# Exit script if file/directory exists
# Run as: protect_filename $FILENAME
protect_filename() {
    local FILENAME="${1}"
    if [[ -e ${FILENAME} ]]; then
        log ERROR "Filename ${FILENAME} already in use"
        exit 1
    fi
}

# Exit script if file/directory does NOT exist
# Run as: check_filename $FILENAME
check_filename() {
    local FILENAME="${1}"
    if [[ ! -e ${FILENAME} ]]; then
        log ERROR "Filename ${FILENAME} does not exist"
        exit 1
    fi
}

#################################
# User input
#################################
SAMPLES="${1}" # File containing sample ids
ZIP_NAME="${2:-aldy_results_$(date +%y%m%d-%H%M%S).7z}"
PHARMACOLYZER_CONFIG="${3:-${SCRIPT_DIR}/../config/tsd_pharmacolyzer_config.json}"
DRYRUN="${4:-}"

check_filename "${PHARMACOLYZER_CONFIG}"

ALDY_RESULT_PATH="$(jq -r .aldy_result_path ${PHARMACOLYZER_CONFIG})"
check_filename "${ALDY_RESULT_PATH}"

check_password "${PASSWORD:-}"

MAKE_ZIP=true
ZIP_CONTENTS=""
# Loop over all input sample ids, making sure to remove Windows-style line endings
for sample_id in $(sed 's/\r$//' "${SAMPLES}" | paste -s -d " " -); do
    CMD="grep -Po 'Job \\d+ completed at \\w+ \\w+ \\d+ \\d+:\\d+:\\d+ \\w+ \\d+' ${ALDY_RESULT_PATH}/*${sample_id}*.log"
    DRYRUN="" run_cmd "eval ${CMD}" && MISSING=0 || MISSING="$?"

    # Check if the SLURM process has completed
    if [[ ${MISSING} -eq 0 ]]; then
        log INFO "SLURM process for sample ${sample_id} has completed"
        # Check that the ALDY result file exists
        check_filename ${ALDY_RESULT_PATH}/*${sample_id}*.aldy

        # Check the exit code of ALDY
        CMD="grep -Po 'INFO:\\d+-\\d+:Run finished with exit code \K\d+' ${ALDY_RESULT_PATH}/*${sample_id}*.log"
        FAILED="$(eval ${CMD})"

        if [[ ${FAILED} -eq 0 ]]; then
            log INFO "Adding ALDY results for sample ${sample_id} to the archive content"
            ZIP_CONTENTS="${ZIP_CONTENTS} $(ls ${ALDY_RESULT_PATH}/*${sample_id}*.{log,aldy})"
        else
            log ERROR "ALDY run for sample ${sample_id} failed with exit code: ${FAILED}"
            MAKE_ZIP=false
        fi
    elif [[ ${MISSING} -eq 1 ]]; then
        log ERROR "SLURM process for sample ${sample_id} has not completed yet"
        MAKE_ZIP=false
    elif [[ ${MISSING} -eq 2 ]]; then
        log ERROR "SLURM process for sample ${sample_id} has not started yet"
        MAKE_ZIP=false
    else
        log ERROR "Unknown error for sample ${sample_id}. Error code from 'grep': ${MISSING}"
        MAKE_ZIP=false
    fi

done

# Make zip file containing all aldy results
if [[ ${MAKE_ZIP} == true ]]; then
    protect_filename "${ZIP_NAME}"
    CMD="7z a -p${PASSWORD:-} -mhe=on ${ZIP_NAME} ${ZIP_CONTENTS}"
    DRYRUN=${DRYRUN:-} run_cmd "${CMD}"
    log INFO "Created zip file: ${ZIP_NAME}"
else
    log ERROR "Not all SLURM processes have completed correctly yet. No zip file created."
fi