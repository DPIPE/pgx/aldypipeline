#!/bin/bash

set -euo pipefail # exit on errors

# Run script as
SCRIPT_NAME="${0}"
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
RUN_COMMAND="bash ${SCRIPT_NAME} [--BAM=bam_cram_file] [--OUTDIR=output_dir]"

#####################
# Internal functions
#####################
# Log to STDERR
# Run as: log [INFO|DEBUG|WARNING|etc] [ERR_MESSAGE]
log() {
    local TYPE="${1}"
    local MSG="${2}"
    local TIME
    TIME="$(date +%y%m%d-%H%M%S)"
    echo "${TYPE}:${TIME}:${MSG}" >&2
}

# Run command (optionally dryun)
# Run as: DRYRUN=${DRYRUN:-} run_cmd $CMD
run_cmd() {
    local CMD="${*}"
    if [[ -z ${DRYRUN} ]]; then
        log INFO "Running: ${CMD}"
        ${CMD}
        return ${?}
    else
        log DRYRUN "${CMD}"
        return 0
    fi
}

# Exit script if environmental variable is not set
# Run as: check_input $VARIABLE
check_input() {
    local INPUT="${1+x}"
    if [[ -z ${INPUT} ]]; then
        log ERROR "Run script as:"
        log ERROR "${RUN_COMMAND}"
        exit 1
    fi
}

# Exit script if file/directory exists
# Run as: protect_filename $FILENAME
protect_filename() {
    local FILENAME="${1}"
    if [[ -e ${FILENAME} ]]; then
        log ERROR "Filename ${FILENAME} already in use"
        exit 1
    fi
}

# Exit script if file/directory does NOT exist
# Run as: check_filename $FILENAME
check_filename() {
    local FILENAME="${1}"
    if [[ ! -e ${FILENAME} ]]; then
        log ERROR "Filename ${FILENAME} does not exist"
        exit 1
    fi
}

# Write array line-separated to STDOUT
# Run as: write_msg $ARRAY[@]
write_msg() {
    local ARRAY=("${@}")
    printf '%s\n' "${ARRAY[@]}"
}

####################
# Runtime variables
####################
# Parse user input
while [ "$#" -gt 0 ]; do
  case "$1" in

    --BAM=*) BAM="${1#*=}"; shift 1;;
    --OUTDIR=*) OUTDIR="${1#*=}"; shift 1;;
    
    --CONFIG=*) CONFIG="${1#*=}"; shift 1;;

    --DRYRUN) DRYRUN="True"; shift 1;;

    *) log ERROR "unknown input: $1"; exit 1;;
  esac
done

# Check that all requirements have been set
check_input ${BAM:-}
check_input ${OUTDIR:-}
check_input ${CONFIG:-}
# Check that required inputs are accessible
check_filename ${BAM}
check_filename ${OUTDIR}
check_filename ${CONFIG}

# Check that jq is available
command -v jq >/dev/null 2>&1 || log ERROR "Command 'jq' was not found"

######################
# Config
######################
SINGULARITY_SCRIPT_EXECUTOR="$(jq -r .singularity_script_executor ${CONFIG})"
log INFO "Using singularity script executor: ${SINGULARITY_SCRIPT_EXECUTOR}"
[[ ${SINGULARITY_SCRIPT_EXECUTOR} =~ ^(sbatch|bash)$ ]] || check_input

SINGULARITY_SCRIPT="$(jq -r .singularity_script ${CONFIG})"
log INFO "Using singularity script: ${SINGULARITY_SCRIPT}"
SINGULARITY_SCRIPT="${SCRIPT_DIR}/$(basename ${SINGULARITY_SCRIPT})"
check_filename ${SINGULARITY_SCRIPT}

ALDY_PROFILE="$(jq -r .aldy_profile ${CONFIG})"
log INFO "Using aldy profile: ${ALDY_PROFILE}"
check_input ${ALDY_PROFILE:-}

REFDATA_PATH="$(realpath $(jq -r .refdata_path ${CONFIG}))"
GENOME_REFERENCE="${REFDATA_PATH}/$(jq -r .genome_reference ${CONFIG})"
GENOME_REFERENCE_PATH="$(dirname ${GENOME_REFERENCE})"
check_filename ${GENOME_REFERENCE}

SINGULARITY_PATH="$(realpath $(jq -r .singularity_path ${CONFIG}))"
SINGULARITY="${SINGULARITY_PATH}/$(jq -r .aldy_container ${CONFIG})"
check_filename ${SINGULARITY}

# Set slurm arguments
SLURM_JOB_NAME="pgxAldy"
SLURM_ACCOUNT="p22_tsd"
SLURM_TIME_REQUEST="1:30:00"
SLURM_CPUS_PER_TASK_REQUEST="2"
SLURM_MEM_PER_CPU_REQUEST="7G"
SLURM_ARGS="--job-name=${SLURM_JOB_NAME} 
--account=${SLURM_ACCOUNT} 
--time=${SLURM_TIME_REQUEST} 
--cpus-per-task=${SLURM_CPUS_PER_TASK_REQUEST} 
--mem-per-cpu=${SLURM_MEM_PER_CPU_REQUEST}"

######################
# Naming conventions
######################
BAM_FILE="$(realpath ${BAM})"
BAM_DIR="$(dirname ${BAM_FILE})"
BAM_BASENAME="$(basename ${BAM_FILE})"
BASENAME="$(basename ${BAM_BASENAME%%.*})"

# Output
OUTDIR="$(realpath ${OUTDIR})"
ALDY_REPORT="${OUTDIR}/${BASENAME}.aldy"
protect_filename "${ALDY_REPORT}"
ALDY_LOG="${OUTDIR}/${BASENAME}.log"
protect_filename "${ALDY_LOG}"

# Run aldy
ALDY_ENV="${SINGULARITY_SCRIPT} exec -H ${OUTDIR} -B ${BAM_DIR}:/aldy:ro,${GENOME_REFERENCE_PATH} $SINGULARITY"

log INFO "Aldy apptainer environment: ${ALDY_ENV}"

ALDY_CMD="aldy 
genotype 
-p ${ALDY_PROFILE} 
--reference ${GENOME_REFERENCE} 
-o ${ALDY_REPORT} 
/aldy/${BAM_BASENAME}"

log INFO "Aldy command: ${ALDY_CMD}"

if [[ "${SINGULARITY_SCRIPT_EXECUTOR}" == "sbatch" ]]; then
  EXECUTOR_CMD="sbatch ${SLURM_ARGS} --output ${ALDY_LOG} ${ALDY_ENV} ${ALDY_CMD}"
else
  EXECUTOR_CMD="bash ${ALDY_ENV} ${ALDY_CMD}"
fi

log INFO "Complete run command: ${EXECUTOR_CMD}"
DRYRUN=${DRYRUN:-} run_cmd ${EXECUTOR_CMD}

# Next steps
NEXT_STEPS=(
  "Next steps:"
  "Transfer ${ALDY_REPORT} to K:/Sensitive"
)

write_msg "${NEXT_STEPS[@]}"