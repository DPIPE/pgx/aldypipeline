#!/usr/bin/env bash

# This script runs Aldy on files
# Example run
# ./pharmacolyze.sh Diag-*HG002* /tsd/p22/data/durable/production/ella/ella-staging/data/analyses/imported

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Exit on errors
set -euo pipefail

#####################
# Internal functions
#####################
# Log to STDERR
# Run as: log [INFO|DEBUG|WARNING|etc] [ERR_MESSAGE]
log() {
    local TYPE="${1}"
    local MSG="${2}"
    local TIME
    TIME="$(date +%y%m%d-%H%M%S)"
    echo "${TYPE}:${TIME}:${MSG}" >&2
}

# Run command (optionally dryun)
# Run as: DRYRUN=${DRYRUN:-} run_cmd $CMD
run_cmd() {
    local CMD="${*}"
    if [[ -z ${DRYRUN} ]]; then
        log INFO "Running: ${CMD}"
        ${CMD}
        return ${?}
    else
        log DRYRUN "${CMD}"
        return 0
    fi
}

# Exit script if environmental variable is not set
# Run as: check_input $VARIABLE
check_input() {
    local INPUT="${1+x}"
    if [[ -z ${INPUT} ]]; then
        log ERROR "Environmental variable not set"
        exit 1
    fi
}

# Exit script if file/directory exists
# Run as: protect_filename $FILENAME
protect_filename() {
    local FILENAME="${1}"
    if [[ -e ${FILENAME} ]]; then
        log ERROR "Filename ${FILENAME} already in use"
        exit 1
    fi
}

# Exit script if file/directory does NOT exist
# Run as: check_filename $FILENAME
check_filename() {
    local FILENAME="${1}"
    if [[ ! -e ${FILENAME} ]]; then
        log ERROR "Filename ${FILENAME} does not exist"
        exit 1
    fi
}

# Write array line-separated to STDOUT
# Run as: write_msg $ARRAY[@]
write_msg() {
    local ARRAY=("${@}")
    printf '%s\n' "${ARRAY[@]}"
}



#################################
# User input
#################################
SAMPLES="${1}"
PHARMACOLYZER_CONFIG="${2:-${SCRIPT_DIR}/../config/tsd_pharmacolyzer_config.json}"
DRYRUN="${3:-}"

check_filename "${PHARMACOLYZER_CONFIG}"
check_input "${DRYRUN:-}"


#################################
# PARSE AND VALDIATE CONFIG
#################################
ALDY_RUNNER="$(jq -r .aldy_runner ${PHARMACOLYZER_CONFIG})"
check_filename "${ALDY_RUNNER}"
ALDY_CONFIG="$(jq -r .aldy_config ${PHARMACOLYZER_CONFIG})"
check_filename "${ALDY_CONFIG}"
ALDY_PIPELINE_VERSION="$(jq -r .aldy_pipeline_version ${PHARMACOLYZER_CONFIG})"
check_filename "${ALDY_PIPELINE_VERSION}"
ALDY_RESULT_PATH="$(jq -r .aldy_result_path ${PHARMACOLYZER_CONFIG})"
check_filename "${ALDY_RESULT_PATH}"
ALDY_SAMPLES="$(jq -r .aldy_samples ${PHARMACOLYZER_CONFIG})"
check_filename "$(dirname ${ALDY_SAMPLES})"
CMD="touch ${ALDY_SAMPLES}"
log INFO "Ensure file existence: ${CMD}"
${CMD}
SAMPLES_JSON="$(jq -r .samples_json ${PHARMACOLYZER_CONFIG})"
check_filename "${SAMPLES_JSON}"

#################################
# START PHARMACOLYSIS
#################################
# Loop over all input sample ids, making sure to remove Windows-style line endings
for sample_id in $(sed 's/\r$//' "${SAMPLES}" | paste -s -d " " -); do

    sample_regex_pattern="${sample_id}.*-DR\$"
    # Check if a sample name containing the sample_id has been previously analysed
    # Assuming that input contains the full sample_id that is the last part of the sample_name
    CMD="grep ${sample_id} ${ALDY_SAMPLES}"
    log INFO "Check that sample has not been previously analysed: ${CMD}"
    ${CMD} >/dev/null && IS_NEW_SAMPLE=0 || IS_NEW_SAMPLE=$?

    # Get the sample name for the sample_id
    sample_name=$(jq -r "to_entries | .[] | select(.key | test(\"$sample_regex_pattern\")) | .key" $SAMPLES_JSON)

    # Get corresponding '.bam' or '.crumble.cram' associated with the sample name if:
    # 1. Aldy was not previously run for this sample
    # 2. The sample name is defined and unique
    if [ "${IS_NEW_SAMPLE}" -eq 0 ]; then
        log WARNING "Sample id ${sample_id} is already present in ${ALDY_SAMPLES}"
        bam="PREVIOUSLY_ANALYZED"
    elif [ -z "${sample_name}" ]; then
        log WARNING "Sample name not found for sample id: ${sample_id}"
        bam="MISSING_SAMPLE_NAME"
    elif [ "$(echo "${sample_name}" | wc -l)" -gt 1 ]; then
        log WARNING "Sample name for sample id ${sample_id} is not unique: ${sample_name}"
        bam="NON_UNIQUE_SAMPLE_NAME"
    else
        log INFO "Found unique sample name for sample id ${sample_id}: ${sample_name}"
        bam=$(jq -r "to_entries | .[] | select(.key | test(\"$sample_regex_pattern\")) | .value.bam" $SAMPLES_JSON)
        log INFO "Found corresponding bam/crumble.cram: ${bam}"
    fi

    # Run ALDY if sample was not previously analysed and unique bam/crumble.cram exists
    if [ -f "${bam}" ]; then
        CMD="bash ${ALDY_RUNNER} --BAM=$bam --OUTDIR=${ALDY_RESULT_PATH} --CONFIG=${ALDY_CONFIG} ${DRYRUN}"
        log INFO "Using $(basename ${ALDY_RUNNER}) from aldypipeline archive: $(cat ${ALDY_PIPELINE_VERSION})"
        log INFO "Running: ${CMD}"
        ${CMD}

        CMD="echo ${sample_name}"
        if [ -z "${DRYRUN}" ]; then
            log INFO "Add to metadata: ${CMD} >>${ALDY_SAMPLES}"
            ${CMD} >>"${ALDY_SAMPLES}"
        else
            log DRYRUN "Add to metadata: ${CMD} >>${ALDY_SAMPLES}"
        fi
    else
        log WARNING "Skipping sample ${sample_id} because no bam: ${bam}"
    fi
done





