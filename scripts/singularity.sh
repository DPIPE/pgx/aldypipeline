#!/bin/bash

JOBSETUP="/cluster/bin/jobsetup"
command -v module >/dev/null 2>&1 || MISSING_MODULE=$?
command -v cleanup >/dev/null 2>&1 || MISSING_CLEANUP=$?
command -v singularity >/dev/null 2>&1 || MISSING_SINGULARITY=$?

if [[ -f ${JOBSETUP} ]]; then
  source ${JOBSETUP}
fi
# Load singularity as module
if [[ -z "${MISSING_MODULE+x}" ]]; then
  module purge   # clear any inherited modules
  if [[ -n "${MISSING_SINGULARITY+x}" ]]; then
    module load singularity/3.7.3  # import singularity
  fi
fi

set -euo pipefail # exit on errors

# Run script as
# SCRIPT_NAME="${0}"
# SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
# RUN_COMMAND="[bash|sbatch] ${SCRIPT_NAME} [singularity arguments]"

#####################
# Internal functions
#####################
# Log to STDERR
# Run as: log [INFO|DEBUG|WARNING|etc] [ERR_MESSAGE]
log() {
    local TYPE="${1}"
    local MSG="${2}"
    local TIME
    TIME="$(date +%y%m%d-%H%M%S)"
    echo "${TYPE}:${TIME}:${MSG}" >&2
}

###########################
# Run singularity on input
###########################
log INFO "Using singularity version $(singularity --version)"
CMD="singularity ${*}"
log INFO "Runnning command: ${CMD}"
${CMD}
log INFO "Run finished with exit code $?"